export default interface IUserDto {
  id: string
  name: string
  lastname: string
  nickname: string
  address: string
  bio: string
  created_at: Date
  updated_at: Date
}