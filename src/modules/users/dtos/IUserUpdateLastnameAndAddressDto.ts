export default interface IUserUpdateLastnameAndAddressDto {
  lastname: string
  address: string
}