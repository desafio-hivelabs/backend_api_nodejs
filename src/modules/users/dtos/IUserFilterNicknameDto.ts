export default interface IUserFilterNicknameDto {
  name: string
  lastname: string
  nickname: string
}