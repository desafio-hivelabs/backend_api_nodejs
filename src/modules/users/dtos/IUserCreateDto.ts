export default interface IUserCreateDto {
  name: string
  lastname: string
  nickname: string
  address: string
  bio?: string
}