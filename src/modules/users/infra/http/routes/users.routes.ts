import { Router } from 'express';
import UserController from '../controllers/UserController';

const usersRouter = Router();
const usersController = new UserController();

usersRouter.get('/', usersController.findAll);
usersRouter.get('/filter', usersController.filterQuery);
usersRouter.get('/filterNickname', usersController.filterByNinckname);
usersRouter.post('/', usersController.create);
usersRouter.put('/updateLastnameAndAddres/:id', usersController.updateLastnameAndAddress);
usersRouter.put('/updateNickname/:id', usersController.updateNickname);
usersRouter.delete('/:id', usersController.delete);

export default usersRouter;
