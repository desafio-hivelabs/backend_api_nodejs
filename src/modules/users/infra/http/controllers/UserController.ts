import { container } from 'tsyringe';
import { Request, Response } from "express"
import UserFindAllService from '@modules/users/services/userFindAll.service';
import UserCreateService from '@modules/users/services/userCreate.service';
import IUserCreateDto from '@modules/users/dtos/IUserCreateDto';
import UserDeleteService from '@modules/users/services/userDelete.service';
import UserFilterAllService from '@modules/users/services/userFilterAll.service';
import FindOneByNinckname from '@modules/users/services/userFindOneByNinckname.service';
import IUserUpdateLastnameAndAddressDto from '@modules/users/dtos/IUserUpdateLastnameAndAddressDto';
import UserUpdateLastnameAndAddresService from '@modules/users/services/userUpdateLastnameAndAddres.service';
import IUserUpdateNicknameDto from '@modules/users/dtos/IUserUpdateNicknameDto';
import UserUpdateNicknameService from '@modules/users/services/userUpdateNickname.service';

export default class UserController {

  public async findAll(req: Request, res: Response): Promise<Response> {
    const userFindAllService = container.resolve(UserFindAllService)
    const users = await userFindAllService.execute()
    return res.status(200).json({ data: users })
  }

  public async filterQuery(req: Request, res: Response): Promise<Response> {
    const { name, lastname }: any = req.query
    const userFilterAllService = container.resolve(UserFilterAllService)
    const users = await userFilterAllService.execute({ lastname, name })
    return res.status(200).json({ data: users })
  }

  public async filterByNinckname(req: Request, res: Response): Promise<Response> {
    const { nickname }: any = req.query
    const findOneByNinckname = container.resolve(FindOneByNinckname)
    const user = await findOneByNinckname.execute(nickname)
    return res.status(200).json({ data: user })
  }

  public async create(req: Request, res: Response): Promise<Response> {
    const data: IUserCreateDto = req.body
    console.log("DATA => ", data)
    const userFindAllService = container.resolve(UserCreateService)
    const user = await userFindAllService.execute(data)
    return res.status(200).json({ data: user })
  }

  public async updateLastnameAndAddress(req: Request, res: Response): Promise<Response> {
    const { address, lastname }: IUserUpdateLastnameAndAddressDto = req.body
    const id: string = req.params['id']
    const userUpdateLastnameAndAddresService = container.resolve(UserUpdateLastnameAndAddresService)
    const user = await userUpdateLastnameAndAddresService.execute(id, { address, lastname })
    return res.status(200).json({ data: user })
  }

  public async updateNickname(req: Request, res: Response): Promise<Response> {
    const { nickname }: IUserUpdateNicknameDto = req.body
    const id: string = req.params['id']
    const userUpdateNicknameService = container.resolve(UserUpdateNicknameService)
    const user = await userUpdateNicknameService.execute(id, { nickname })
    return res.status(200).json({ data: user })
  }

  public async delete(req: Request, res: Response): Promise<Response> {
    const id: string = req.params['id']
    console.log(id)
    const userFindAllService = container.resolve(UserDeleteService)
    await userFindAllService.execute(id)
    return res.status(204).json({ data: "Delerado com sucesso" })
  }
}