import IUserCreateDto from '@modules/users/dtos/IUserCreateDto';
import IUserDto from '@modules/users/dtos/IUserDto';
import IUserUpdateNicknameDto from '@modules/users/dtos/IUserUpdateNicknameDto';
import { getRepository, Repository, Not, Like, Raw } from 'typeorm';
import User from '../entities/User';
import IUserRepository from './IUserRepository';

export default class UserRepository implements IUserRepository {
  private ormRepository: Repository<User>;

  constructor() {
    this.ormRepository = getRepository(User);
  }

  public async find(): Promise<User[]> {
    const user = await this.ormRepository.find()
    return user
  }

  public async findById(id: string): Promise<IUserDto | undefined> {
    const user = await this.ormRepository.findOne({ where: { id } });
    return user;
  }

  public async findOneByNickname(nickname: string): Promise<IUserDto | undefined> {
    const user = await this.ormRepository.findOne({
      select: ['name', 'lastname', 'nickname'],
      where: { nickname: nickname }
    });
    return user;
  }

  public async filterQuery({ name, lastname }: any): Promise<IUserDto[] | undefined> {
    const where = {
      where: [{
        name: Like(name),
      }, {
        lastname: Like(lastname),
      }]
    }
    const user = await this.ormRepository.find(where);
    return user;
  }

  public async updateLastnameAndAddress(user: any, { lastname, address }: any): Promise<IUserDto> {

    this.ormRepository.merge(user, { lastname, address })
    await this.ormRepository.save(user);
    return user;
  }

  public async updateNickname(user: any, { nickname }: IUserUpdateNicknameDto): Promise<IUserDto> {
    this.ormRepository.merge(user, { nickname })
    await this.ormRepository.save(user);
    return user;
  }

  public async create(userData: IUserCreateDto): Promise<IUserDto> {
    const user = this.ormRepository.create(userData);
    await this.ormRepository.save(user);
    return user;
  }

  public async delete(id: string): Promise<void> {
    await this.ormRepository.delete(id);
  }

  public async save(user: User): Promise<void> {
    this.ormRepository.save(user);
  }
}

