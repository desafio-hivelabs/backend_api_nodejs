import IUserCreateDto from '@modules/users/dtos/IUserCreateDto';
import IUserDto from '@modules/users/dtos/IUserDto';
import IUserFilterNicknameDto from '@modules/users/dtos/IUserFilterNicknameDto';
import IUserUpdateNicknameDto from '@modules/users/dtos/IUserUpdateNicknameDto';
import User from '../entities/User';

export default interface IUserRepository {
  create(data: IUserCreateDto): Promise<IUserDto>;

  find(): Promise<User[]>

  findById(id: string): Promise<IUserDto | undefined>

  findOneByNickname(nickname: string): Promise<IUserFilterNicknameDto | undefined>

  filterQuery({ name, lastname }: any): Promise<User[] | undefined>

  updateLastnameAndAddress(user: any, { lastname, address }: any): Promise<IUserDto>

  updateNickname(user: any, { nickname }: IUserUpdateNicknameDto): Promise<IUserDto>

  create(userData: IUserCreateDto): Promise<IUserDto>

  delete(id: string): Promise<void>

  save(user: User): Promise<void>
}