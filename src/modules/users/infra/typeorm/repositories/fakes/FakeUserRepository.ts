import IUserCreateDto from '@modules/users/dtos/IUserCreateDto';
import IUserDto from '@modules/users/dtos/IUserDto';
import IUserFilterNicknameDto from '@modules/users/dtos/IUserFilterNicknameDto';
import IUserUpdateNicknameDto from '@modules/users/dtos/IUserUpdateNicknameDto';
import { v4 as uuidv4 } from 'uuid';
import User from '../../entities/User';
import IUserRepository from '../IUserRepository';


export default class FakeUserRepository implements IUserRepository {
  private users: User[] = [];

  constructor() {
  }

  public async find(): Promise<User[]> {
    const user = this.users
    return user
  }

  public async findById(id: string): Promise<IUserDto | undefined> {
    const user = this.users.find((user: IUserDto) => user.id)
    return user;
  }

  public async findOneByNickname(nickname: string): Promise<IUserDto | undefined> {
    const user = this.users.find((user: IUserFilterNicknameDto) => user.nickname == nickname);
    return user;
  }

  public async filterQuery({ name, lastname }: any): Promise<IUserDto[]> {
    const users = this.users.filter((user: IUserDto) => user.name == name || user.lastname == lastname);
    return users;
  }

  public async updateLastnameAndAddress(user: any, { lastname, address }: any): Promise<IUserDto> {
    Object.assign(user, { lastname, address })
    await this.save(user);
    return user;
  }

  public async updateNickname(user: any, { nickname }: IUserUpdateNicknameDto): Promise<IUserDto> {
    Object.assign(user, { nickname })
    await this.save(user);
    return user;
  }

  public async create(userData: IUserCreateDto): Promise<IUserDto> {
    const user = new User()

    user.id = uuidv4()
    user.name = userData.name
    user.lastname = userData.lastname
    user.nickname = userData.nickname
    user.address = userData.address
    user.bio = `${userData.bio}`
    user.created_at = new Date()
    user.updated_at = new Date()
    this.save(user)
    return user;
  }

  public async delete(id: string): Promise<void> {
    const users = this.users.filter((user) => { return user.id != id });
    this.users = users
  }

  public async save(user: IUserDto): Promise<void> {
    this.users.push(user)
  }
}

