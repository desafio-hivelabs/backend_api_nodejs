import FakeUserRepository from '@modules/users/infra/typeorm/repositories/fakes/FakeUserRepository';
import IUserCreateDto from '../dtos/IUserCreateDto';
import UserCreateService from "./userCreate.service";
import UserFilterAllService from './userFilterAll.service';


describe('UserFilterAll', () => {

  it('should list all users filtered by the first and/or lastname', async () => {
    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const userFilterAllService = new UserFilterAllService(fakeUserRepository)

    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab31',
      address: 'addres1',
      bio: 'massa',
    }

    const data2: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista2',
      nickname: 'raphab32',
      address: 'addres2',
      bio: 'massa',
    }

    await userCreateService.execute(data)
    await userCreateService.execute(data2)

    const query1 = {name: 'rafael', lastname: ''}
    const users1 = await userFilterAllService.execute(query1)
    expect(users1.length).toEqual(2);

    const query2 = {name: '', lastname: 'Batista2'}
    const users2 = await userFilterAllService.execute(query2)
    expect(users2.length).toEqual(1);
  })
})
