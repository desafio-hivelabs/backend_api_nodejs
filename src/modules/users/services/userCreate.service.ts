import { inject, injectable } from "tsyringe";

import ResponseError from "@shared/errors/responseErrors";
import IUserRepository from "@modules/users/infra/typeorm/repositories/IUserRepository";
import IUserCreateDto from "../dtos/IUserCreateDto";
import IUserDto from "../dtos/IUserDto";


@injectable()
export default class UserCreateService {

  constructor(
    @inject('UserRepository')
    private userRepository: IUserRepository
  ) { }

  async execute({ name, lastname, nickname, address, bio }: IUserCreateDto): Promise<IUserDto> {

    if (nickname.length > 30) throw new ResponseError("Exceeded limit of characters max: 30");
    const nicknamefound = await this.userRepository.findOneByNickname(nickname);

    if (nicknamefound) {
      throw new ResponseError("conflict! Nickname exists", 409);
    }

    const user = this.userRepository.create({ name, lastname, nickname, address, bio })
    return user
  }

}