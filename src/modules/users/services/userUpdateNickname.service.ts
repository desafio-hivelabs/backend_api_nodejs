import ResponseError from "@shared/errors/responseErrors";
import { inject, injectable } from "tsyringe";
import IUserDto from "../dtos/IUserDto";
import IUserUpdateNicknameDto from "../dtos/IUserUpdateNicknameDto";
import IUserRepository from "../infra/typeorm/repositories/IUserRepository";

@injectable()
export default class UserUpdateNicknameService {

  constructor(
    @inject('UserRepository')
    private userRepository: IUserRepository
  ) { }


  async execute(id: string, { nickname }: IUserUpdateNicknameDto): Promise<IUserDto> {
    if (nickname.length > 30) throw new ResponseError("Exceeded limit of characters max: 30", 400);

    const userfound = await this.userRepository.findById(id)
    if (!userfound) throw new ResponseError("User not found", 404);
    
   
    const nicknamefound = await this.userRepository.findOneByNickname(nickname);
    if (nicknamefound) throw new ResponseError("conflict! Nickname exists", 409);

    const user = await this.userRepository.updateNickname(userfound, { nickname })
    return user
  }
}