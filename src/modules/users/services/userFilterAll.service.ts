import { inject, injectable } from "tsyringe";
import IUserDto from "../dtos/IUserDto";
import IUserRepository from "../infra/typeorm/repositories/IUserRepository";

interface IRequest {
  name: string,
  lastname: string
}

@injectable()
export default class UserFilterAllService {

  constructor(
    @inject('UserRepository')
    private userRepository: IUserRepository
  ) { }

  async execute({ name, lastname }: IRequest): Promise<IUserDto[] | any> {

    const users = this.userRepository.filterQuery({ name, lastname })
    return users
  }

}