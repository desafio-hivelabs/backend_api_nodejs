import { inject, injectable } from "tsyringe";
import User from "../infra/typeorm/entities/User";
import IUserRepository from "../infra/typeorm/repositories/IUserRepository";


@injectable()
export default class UserFindAllService {

  constructor(
    @inject('UserRepository')
    private userRepository: IUserRepository
  ) { }

  async execute(): Promise<User[]> {
    const users = this.userRepository.find()
    return users
  }

}