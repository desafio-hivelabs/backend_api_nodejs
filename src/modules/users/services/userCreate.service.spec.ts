import FakeUserRepository from '@modules/users/infra/typeorm/repositories/fakes/FakeUserRepository';
import ResponseError from '@shared/errors/responseErrors';
import IUserCreateDto from '../dtos/IUserCreateDto';
import User from '../infra/typeorm/entities/User';
import UserCreateService from "./userCreate.service";


describe('UserCreate', () => {

  it('should not allow creating a user a nickname greater than 30', async () => {
    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const user: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab3311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111',
      address: 'okokok',
      bio: 'massa',
    }

    expect(userCreateService.execute(user)).rejects.toBeInstanceOf(ResponseError);
  })

  it('should not create a user with the same nickname', async () => {

    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const user: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab32',
      address: 'okokok',
      bio: 'massa',
    }

    await userCreateService.execute(user)
    expect(userCreateService.execute(user)).rejects.toBeInstanceOf(ResponseError);

  })

  it('should create a new user', async () => {
    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab33',
      address: 'okokok',
      bio: 'massa',
    }

    const user = await userCreateService.execute(data)

    expect(user).toHaveProperty('id')
    expect(user).toBeInstanceOf(User)
    expect(user).toHaveProperty('created_at')
    expect(user).toHaveProperty('updated_at')
  })

})