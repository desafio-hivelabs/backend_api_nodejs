import { inject, injectable } from "tsyringe";
import IUserDto from "../dtos/IUserDto";
import IUserUpdateLastnameAndAddressDto from "@modules/users/dtos/IUserUpdateLastnameAndAddressDto";
import ResponseError from "@shared/errors/responseErrors";
import IUserRepository from "../infra/typeorm/repositories/IUserRepository";

@injectable()
export default class UserUpdateLastnameAndAddresService {

  constructor(
    @inject('UserRepository')
    private userRepository: IUserRepository
  ) { }


  async execute(id: string, { lastname, address }: IUserUpdateLastnameAndAddressDto): Promise<IUserDto> {

    const userFound = await this.userRepository.findById(id)
    if (!userFound) {
      throw new ResponseError("User not found", 404);
    }

    const user = await this.userRepository.updateLastnameAndAddress(userFound, { lastname, address })
    return user
  }
}