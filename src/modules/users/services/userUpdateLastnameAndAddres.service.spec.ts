import FakeUserRepository from '@modules/users/infra/typeorm/repositories/fakes/FakeUserRepository';
import ResponseError from '@shared/errors/responseErrors';
import IUserCreateDto from '../dtos/IUserCreateDto';
import UserCreateService from "./userCreate.service";
import UserUpdateLastnameAndAddresService from './userUpdateLastnameAndAddres.service';


describe('UserUpdateLastnameAndAddres', () => {

  it('should show a user filtered by nickname', async () => {
    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const userUpdateLastnameAndAddresService = new UserUpdateLastnameAndAddresService(fakeUserRepository)

    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab33',
      address: 'okokok',
      bio: 'massa',
    }

    const user = await userCreateService.execute(data)
    const query = { lastname: 'Santos', address: 'aaaaaaa' }
    const userUpdated = await userUpdateLastnameAndAddresService.execute(user.id, query)
    expect(userUpdated.lastname).toContain('Santos');
    expect(userUpdated.address).toContain('aaaaaaa');
  })


  it('should show an error when fetching id that doesnt exist', async () => {
    const fakeUserRepository = new FakeUserRepository()
    const userUpdateLastnameAndAddresService = new UserUpdateLastnameAndAddresService(fakeUserRepository)

    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab33',
      address: 'okokok',
      bio: 'massa',
    }

    const query = { lastname: 'Santos', address: 'aaaaaaa' }
    expect(userUpdateLastnameAndAddresService.execute('123', query)).rejects.toBeInstanceOf(ResponseError);

  })

})
