import ResponseError from "@shared/errors/responseErrors";
import { inject, injectable } from "tsyringe";
import IUserFilterNicknameDto from "../dtos/IUserFilterNicknameDto";
import IUserRepository from "../infra/typeorm/repositories/IUserRepository";

@injectable()
export default class UserFindOneByNincknameService {

  constructor(
    @inject('UserRepository')
    private userRepository: IUserRepository
  ) { }

  async execute(nickname: string): Promise<IUserFilterNicknameDto> {

    const user = await this.userRepository.findOneByNickname(nickname)
    if (!user) {
      throw new ResponseError("User not found", 404);
    }
    return user
  }
}