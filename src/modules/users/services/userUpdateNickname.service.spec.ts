import FakeUserRepository from '@modules/users/infra/typeorm/repositories/fakes/FakeUserRepository';
import ResponseError from '@shared/errors/responseErrors';
import IUserCreateDto from '../dtos/IUserCreateDto';
import UserCreateService from "./userCreate.service";
import UserUpdateNicknameService from './userUpdateNickname.service';

describe('UserUpdateNickname', () => {

  it('should show error when not finding the user', async () => {

    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const userUpdateNicknameService = new UserUpdateNicknameService(fakeUserRepository)

    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab32',
      address: 'okokok',
      bio: 'massa',
    }

    const query = { nickname: 'raphab32' }
    expect(userUpdateNicknameService.execute("123", query)).rejects.toBeInstanceOf(ResponseError);

  })

  it('should not allow update a user a nickname greater than 30', async () => {
    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const userUpdateNicknameService = new UserUpdateNicknameService(fakeUserRepository)
    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab33',
      address: 'okokok',
      bio: 'massa',
    }

    const user = await userCreateService.execute(data)
    const query = { nickname: "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111" }
    expect(userUpdateNicknameService.execute(`${user.id}`, query)).rejects.toBeInstanceOf(ResponseError);
  })

  it('should not update a user with the same nickname', async () => {

    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const userUpdateNicknameService = new UserUpdateNicknameService(fakeUserRepository)

    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab32',
      address: 'okokok',
      bio: 'massa',
    }

    const user = await userCreateService.execute(data)
    const query = { nickname: 'raphab32' }
    expect(userUpdateNicknameService.execute(`${user.id}`, query)).rejects.toBeInstanceOf(ResponseError);

  })

  it('should update a nickname of user', async () => {

    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const userUpdateNicknameService = new UserUpdateNicknameService(fakeUserRepository)

    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab32',
      address: 'okokok',
      bio: 'massa',
    }

    const user = await userCreateService.execute(data)
    const query = { nickname: 'other_nickname' }
    const userUpdated = await userUpdateNicknameService.execute(user.id, query)
    expect(userUpdated.nickname).toBe(`${query.nickname}`)
  })

})
