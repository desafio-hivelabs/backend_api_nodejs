import FakeUserRepository from '@modules/users/infra/typeorm/repositories/fakes/FakeUserRepository';
import IUserCreateDto from '../dtos/IUserCreateDto';
import UserCreateService from "./userCreate.service";
import UserFindAllService from './userFindAll.service';


describe('UserFindAll', () => {

  it('should show all users', async () => {
    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const userFindAllService = new UserFindAllService(fakeUserRepository)

    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab33',
      address: 'okokok',
      bio: 'massa',
    }

    await userCreateService.execute(data)
    const users = await userFindAllService.execute()
    expect(users.length).toEqual(1);
  })
})
