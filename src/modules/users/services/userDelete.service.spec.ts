import FakeUserRepository from '@modules/users/infra/typeorm/repositories/fakes/FakeUserRepository';
import ResponseError from '@shared/errors/responseErrors';
import IUserCreateDto from '../dtos/IUserCreateDto';
import UserCreateService from "./userCreate.service";
import UserDeleteService from './userDelete.service';
import UserFindAllService from './userFindAll.service';


describe('UserDelete', () => {

  it('should show error when trying to delete user that doesnt exist', async () => {
    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const userDeleteService = new UserDeleteService(fakeUserRepository)
    const userFindAllService = new UserFindAllService(fakeUserRepository)

    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab33',
      address: 'okokok',
      bio: 'massa',
    }

    await userCreateService.execute(data)
    const users = await userFindAllService.execute()
    const usercreated = users[0]
    await userDeleteService.execute(usercreated.id)
    expect(userDeleteService.execute(usercreated.id)).rejects.toBeInstanceOf(ResponseError);

  })
})


it('should delete user', async () => {
  const fakeUserRepository = new FakeUserRepository()
  const userCreateService = new UserCreateService(fakeUserRepository)
  const userDeleteService = new UserDeleteService(fakeUserRepository)
  const userFindAllService = new UserFindAllService(fakeUserRepository)

  const data: IUserCreateDto = {
    name: 'rafael',
    lastname: 'Batista',
    nickname: 'raphab33',
    address: 'okokok',
    bio: 'massa',
  }

  await userCreateService.execute(data)
  const users = await userFindAllService.execute()
  const usercreated = users[0]
  await userDeleteService.execute(usercreated.id)
  const newUsers = await userFindAllService.execute()
  expect(newUsers).toEqual([])
})
