import ResponseError from "@shared/errors/responseErrors";
import { inject, injectable } from "tsyringe";
import IUserRepository from "../infra/typeorm/repositories/IUserRepository";



@injectable()
export default class UserDeleteService {

  constructor(
    @inject('UserRepository')
    private userRepository: IUserRepository
  ) { }
  
  async execute(id: string): Promise<void> {
    const user: any = await this.userRepository.findById(id)
    if (!user) {
      throw new ResponseError("User not found", 404);
    }
    await this.userRepository.delete(user.id)
  }
}