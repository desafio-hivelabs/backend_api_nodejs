import FakeUserRepository from '@modules/users/infra/typeorm/repositories/fakes/FakeUserRepository';
import ResponseError from '@shared/errors/responseErrors';
import IUserCreateDto from '../dtos/IUserCreateDto';
import UserCreateService from "./userCreate.service";
import UserFindAllService from './userFindAll.service';
import UserFindOneByNincknameService from './userFindOneByNinckname.service';


describe('UserFindOneByNinckname', () => {

  it('should show a user filtered by nickname', async () => {
    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const userFindOneByNincknameService = new UserFindOneByNincknameService(fakeUserRepository)

    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab33',
      address: 'okokok',
      bio: 'massa',
    }

    const user = await userCreateService.execute(data)
    const userFound = await userFindOneByNincknameService.execute(user.nickname)
    expect(userFound.nickname).toContain('raphab33');
  })

  it('should show an error when fetching nickname that doesnt exist.', async () => {
    const fakeUserRepository = new FakeUserRepository()
    const userCreateService = new UserCreateService(fakeUserRepository)
    const userFindOneByNincknameService = new UserFindOneByNincknameService(fakeUserRepository)

    const data: IUserCreateDto = {
      name: 'rafael',
      lastname: 'Batista',
      nickname: 'raphab33',
      address: 'okokok',
      bio: 'massa',
    }

    await userCreateService.execute(data)
    expect(userFindOneByNincknameService.execute('other_nickname')).rejects.toBeInstanceOf(ResponseError);
  })
})
