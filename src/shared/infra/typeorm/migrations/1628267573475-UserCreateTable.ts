import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class UserCreateTable1628267573475 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'users',
            columns: [
                {
                    name: 'id',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                    isNullable: false,
                },
                {
                    name: 'name',
                    type: 'varchar',
                    isNullable: false
                },
                {
                    name: 'lastname',
                    type: 'varchar',
                    isNullable: false
                },
                {
                    name: 'nickname',
                    isUnique: true,
                    type: 'varchar',
                    length: '30',
                    isNullable: false
                },
                {
                    name: 'address',
                    type: 'varchar',
                    isNullable: false
                },
                {
                    name: 'bio',
                    type: 'varchar',
                    length: '100',
                    isNullable: true
                },
                {
                    name: 'created_at',
                    type: 'timestamp',
                    default: 'now()'
                },
                {
                    name: 'updated_at',
                    type: 'timestamp',
                    default: 'now()'
                }
            ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('users')
    }

}
