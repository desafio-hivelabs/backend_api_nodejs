import "reflect-metadata";
import { createConnection } from 'typeorm'
import '../../container';


createConnection().then(async connection => {
  console.log("DATABASE CONNECTED SUCCESS!");

  process.on('SIGINT', () => {
    connection.close().then(() => console.log("CONEXÃO CLOSED"))
  })

  return connection;

}).catch(error => console.log(error));