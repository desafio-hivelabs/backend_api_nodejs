import "reflect-metadata";
import 'dotenv/config';
import './container';
import '../shared/infra/typeorm'

import swaggerUi from 'swagger-ui-express'
import * as swaggerFile from '@config/swegger/swagger_output.json'
import express from 'express'
import 'express-async-errors';
import path from 'path';

import routes from './infra/routes';
import cors from 'cors';


const app = express();
const PORT = process.env.PORT || 3000;

var corsOptions = {
  origin: "*",
  optionsSuccessStatus: 200
}

app.use((_req, _res, next) => {
  next()
})

// CORS
app.use(cors(corsOptions))

const pageDoc = path.resolve(__dirname, '..', '..', 'frontend-hivelabs');
const pageTest = path.resolve(__dirname, '..', '..', 'coverage', 'lcov-report');



app.use('/', express.static(pageDoc));
app.use('/tests', express.static(pageTest));
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile))

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use("/api/v1", routes)

app.use((error: any, req: any, res: any, next: any) => {
  res.status(error.statusCode).json({ msg: error.message, status: error.statusCode });
})

const server = app.listen(`${PORT}`, () => {
  console.log(
    `🔥 Server started on port => ${PORT} 
  \nSwagger documentation: ${process.env.HOST}:${PORT}/doc
  \nUnitary tests: ${process.env.HOST}:${PORT}/tests
  \n`
  );
});

process.on('SIGINT', () => {
  server.close()
  console.log(`Server closed`);
})